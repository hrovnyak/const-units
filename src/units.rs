#![allow(non_upper_case_globals)]

//! Recognized SI units

use crate::SI;

/// Pi as a fraction
///
/// Its value is the smallest continued fraction where converting to f64s and dividing produces a value exactly equal to f64::consts::PI
pub const PI: (i128, u128) = (245850922, 78256779);

/// A dimensionless value
#[doc(alias = "1")]
pub const DIMENSIONLESS: SI = SI {
    scale: (1, 1),
    s: (0, 1),
    m: (0, 1),
    kg: (0, 1),
    A: (0, 1),
    K: (0, 1),
    mol: (0, 1),
    cd: (0, 1),
};

// Base units

/// Second (s)
#[doc(alias = "s")]
pub const second: SI = SI {
    scale: (1, 1),
    s: (1, 1),
    m: (0, 1),
    kg: (0, 1),
    A: (0, 1),
    K: (0, 1),
    mol: (0, 1),
    cd: (0, 1),
};
/// Meter (m)
#[doc(alias = "m")]
pub const meter: SI = SI {
    scale: (1, 1),
    s: (0, 1),
    m: (1, 1),
    kg: (0, 1),
    A: (0, 1),
    K: (0, 1),
    mol: (0, 1),
    cd: (0, 1),
};
/// Kilogram (kg)
#[doc(alias = "kg")]
pub const kilogram: SI = SI {
    scale: (1, 1),
    s: (0, 1),
    m: (0, 1),
    kg: (1, 1),
    A: (0, 1),
    K: (0, 1),
    mol: (0, 1),
    cd: (0, 1),
};
/// Ampere (A)
#[doc(alias = "A")]
pub const ampere: SI = SI {
    scale: (1, 1),
    s: (0, 1),
    m: (0, 1),
    kg: (0, 1),
    A: (1, 1),
    K: (0, 1),
    mol: (0, 1),
    cd: (0, 1),
};
/// Kelvin (K)
#[doc(alias = "K")]
pub const kelvin: SI = SI {
    scale: (1, 1),
    s: (0, 1),
    m: (0, 1),
    kg: (0, 1),
    A: (0, 1),
    K: (1, 1),
    mol: (0, 1),
    cd: (0, 1),
};
/// Mole (mol)
#[doc(alias = "mol")]
pub const mole: SI = SI {
    scale: (1, 1),
    s: (0, 1),
    m: (0, 1),
    kg: (0, 1),
    A: (0, 1),
    K: (0, 1),
    mol: (1, 1),
    cd: (0, 1),
};
/// Candela (cd)
#[doc(alias = "cd")]
pub const candela: SI = SI {
    scale: (1, 1),
    s: (0, 1),
    m: (0, 1),
    kg: (0, 1),
    A: (0, 1),
    K: (0, 1),
    mol: (0, 1),
    cd: (1, 1),
};

// Named derived units

/// Radian (rad) (effectively an alias for `DIMENSIONLESS`)
#[doc(alias = "rad")]
pub const radian: SI = meter.div(meter);
/// Steradian (sr) (effectively an alias for `DIMENSIONLESS`)
#[doc(alias = "sr")]
pub const steradian: SI = meter.powi(2).div(meter.powi(2));
/// Hertz (Hz)
#[doc(alias = "Hz")]
pub const hertz: SI = second.powi(-1);
/// Newton (N)
#[doc(alias = "N")]
pub const newton: SI = kilogram.mul(meter).div(second.powi(2));
/// Pascal (Pa)
#[doc(alias = "Pa")]
pub const pascal: SI = kilogram.div(meter).div(second.powi(2));
/// Joule (J)
#[doc(alias = "J")]
pub const joule: SI = newton.mul(meter);
/// Watt (W)
#[doc(alias = "W")]
pub const watt: SI = joule.div(second);
/// Coulomb (C)
#[doc(alias = "C")]
pub const coulomb: SI = second.mul(ampere);
/// Volt (V)
#[doc(alias = "V")]
pub const volt: SI = watt.div(ampere);
/// Farad (F)
#[doc(alias = "F")]
pub const farad: SI = coulomb.div(volt);
/// Ohm (Ω)
#[doc(alias = "omega")]
#[doc(alias = "Ω")]
pub const ohm: SI = volt.div(ampere);
/// Siemens (S)
#[doc(alias = "S")]
pub const siemens: SI = ohm.powi(-1);
/// Weber (Wb)
#[doc(alias = "Wb")]
pub const weber: SI = volt.mul(second);
/// Tesla (T)
#[doc(alias = "T")]
pub const tesla: SI = weber.div(meter.powi(2));
/// Henry (H)
#[doc(alias = "H")]
pub const henry: SI = weber.div(ampere);
/// Lumen (lm) (effectively an alias for `candela`)
#[doc(alias = "lm")]
pub const lumen: SI = candela.mul(steradian);
/// Lux (lx)
#[doc(alias = "lx")]
pub const lux: SI = candela.mul(steradian).div(meter.powi(2));
/// Bequerel (Bq) (alias for `hertz`)
#[doc(alias = "Bq")]
pub const bequerel: SI = hertz;
/// Gray (Gy)
#[doc(alias = "Gy")]
pub const gray: SI = joule.div(kilogram);
/// Sievert (Sv) (alias for `gray`)
#[doc(alias = "Sv")]
pub const sievert: SI = gray;
/// Katal (kat)
#[doc(alias = "kat")]
pub const katal: SI = mole.div(second);

/// Percent (%)
#[doc(alias = "%")]
pub const percent: SI = DIMENSIONLESS.scale_by((1, 100));
/// Degree (°)
#[doc(alias = "°")]
pub const degree: SI = DIMENSIONLESS.scale_by(PI).scale_by((1, 180));
/// Arcminute (′)
#[doc(alias = "′")]
pub const arcminute: SI = degree.scale_by((1, 60));
/// Arcsecond (″)
#[doc(alias = "″")]
pub const arcsecond: SI = arcminute.scale_by((1, 60));
/// Minute (min)
#[doc(alias = "min")]
pub const minute: SI = second.scale_by((60, 1));
/// Hour (h)
#[doc(alias = "h")]
pub const hour: SI = meter.scale_by((60, 1));
/// Day (d)
#[doc(alias = "d")]
pub const day: SI = hour.scale_by((24, 1));

/// Astronomical Unit (au)
pub const au: SI = meter.scale_by((149597870700, 1)); // I think using the `au` abbreviation is okay since `astronomical_unit` would be so long and usually people pronounce "a-u" when speaking
/// Hectare (ha)
#[doc(alias = "ha")]
pub const hectare: SI = meter.powi(2).scale_by((10000, 1));

/// Liter (l, L)
#[doc(alias = "l")]
pub const liter: SI = meter.powi(3).scale_by((1, 1000));

/// Metric ton (t)
#[doc(alias = "t")]
pub const ton: SI = kilogram.scale_by((1000, 1));
/// Dalton (Da)
#[doc(alias = "Da")]
pub const dalton: SI = kilogram.scale_by((1_660_539_040, 10_u128.pow(27 + 9)));

/// Electronvolt (eV)
#[doc(alias = "eV")]
pub const electronvolt: SI = joule.scale_by((1_602_176_634, 10_u128.pow(19 + 9)));

// Must be sorted for binary search
pub(crate) const UNITS_MAP: [(&str, SI); 42] = [
    ("%", percent),
    ("A", ampere),
    ("Bq", bequerel),
    ("C", coulomb),
    ("Da", dalton),
    ("F", farad),
    ("Gy", gray),
    ("H", henry),
    ("Hz", hertz),
    ("J", joule),
    ("K", kelvin),
    ("L", liter),
    ("N", newton),
    ("Pa", pascal),
    ("S", siemens),
    ("Sv", sievert),
    ("T", tesla),
    ("V", volt),
    ("W", watt),
    ("Wb", weber),
    ("au", au),
    ("cd", candela),
    ("d", day),
    ("eV", electronvolt),
    ("g", crate::prefixes::milli(kilogram)), // parsing dkg as deci-kilogram would be bad, we only want one prefix
    ("h", hour),
    ("ha", hectare),
    ("kat", katal),
    ("l", liter),
    ("lm", lumen),
    ("lx", lux),
    ("m", meter),
    ("min", minute),
    ("mol", mole),
    ("rad", radian),
    ("s", second),
    ("sr", steradian),
    ("t", ton),
    ("°", degree),
    ("Ω", ohm),
    ("′", arcminute),
    ("″", arcsecond),
];

#[cfg(test)]
mod tests {
    use crate::units::UNITS_MAP;

    #[test]
    fn is_sorted() {
        for i in 0..UNITS_MAP.len() - 1 {
            assert!(UNITS_MAP[i].0 < UNITS_MAP[i + 1].0);
        }
    }
}
