#![allow(incomplete_features)]
#![cfg_attr(feature = "const", feature(generic_const_exprs))]
#![cfg_attr(feature = "const", feature(adt_const_params))]
#![cfg_attr(all(not(test), not(feature = "std")), no_std)]
#![cfg_attr(docsrs, feature(doc_cfg))]

/*!
A library that lets you validate the dimensions of your quantities at compile time and at runtime.

<div class="warning">WARNING: Uses the experimental features `generic_const_exprs` and `adt_const_params` which are known to cause bugs. Disable the `const` feature if you're only interested in runtime checking.</div>

## Compile time example

Okay

```rust
use const_units::si; // Parses units at compile time
use const_units::Quantity; // Represents a number with a dimension

// Input attometers, return square attometers
fn square_dist(
    x: Quantity<f64, { si("am") }>,
    y: Quantity<f64, { si("am") }>,
) -> Quantity<f64, { si("am^2") }> {
    x.powi::<2>() + y.powi::<2>()
}

// Input attometers, return meters
fn distance(
    x: Quantity<f64, { si("am") }>,
    y: Quantity<f64, { si("am") }>,
) -> Quantity<f64, { si("m") }> {
    square_dist(x, y)
        .powf::<{ (1, 2) }>() // `(1, 2)` represents 1/2
        .convert_to::<{ si("m") }>()
}

assert_eq!(
    distance(Quantity(3.), Quantity(4.)),
    Quantity(0.000_000_000_000_000_005)
);
```

Broken

```compile_fail
# use const_units::{Quantity, units::{meter, second, DIMENSIONLESS}};
fn sum(
    x: Quantity<f64, { meter }>,
    y: Quantity<f64, { second }>,
) -> Quantity<f64, DIMENSIONLESS> {
    x + y // You can't add meters and seconds
}
```

## Run time example

Requires the `dyn` feature

```rust
use const_units::si;
use const_units::DynQuantity; // A quantity with dynamic units stored at runtime alongside the number
use const_units::InconsistentUnits; // The error returned when inconsistent units are used together

fn distance(
    x: DynQuantity<f64>,
    y: DynQuantity<f64>,
) -> DynQuantity<f64> {
    // The addition operator will panic if the units are inconsistent
    (x.powi(2) + y.powi(2))
        .powf((1, 2))
        .convert_to(si("m"))
        .unwrap()
}

fn distance_checked(
    x: DynQuantity<f64>,
    y: DynQuantity<f64>,
) -> Result<DynQuantity<f64>, InconsistentUnits> {
    // You can use checked operators to avoid panicking
    x.powi(2)
        .checked_add(y.powi(2))?
        .powf((1, 2))
        .convert_to(si("m"))
}

assert_eq!(
    distance_checked(DynQuantity(3., si("am")), DynQuantity(4., si("am"))),
    Ok(DynQuantity(0.000_000_000_000_000_005, si("m"))),
);
assert_eq!(
    distance_checked(DynQuantity(3., si("m")), DynQuantity(4., si("m"))),
    Ok(DynQuantity(5., si("m"))),
);
assert!(distance_checked(DynQuantity(3., si("m")), DynQuantity(4., si("s"))).is_err());
```

## no-std

The `std` feature can be disabled to allow the crate to function in no-std environments. Doing so will remove methods to convert quantities directly to strings and prevent errors from implementing `std::error::Error`.
*/
use core::fmt::Display;

use units::DIMENSIONLESS;

mod parsing;
pub mod prefixes;
pub mod units;

pub use parsing::*;

#[cfg(feature = "const")]
mod quantity;
#[cfg(feature = "const")]
pub use quantity::*;

#[cfg(feature = "dyn")]
mod dyn_quantity;
#[cfg(feature = "dyn")]
pub use dyn_quantity::*;

// https://stackoverflow.com/questions/7407752/integer-nth-root
const fn try_root(v: u128, n: u32) -> Option<u128> {
    // (2..128).map(|v| (u128::MAX as f64).powf(1. / v as f64).ceil() as u128).collect::<Vec<_>>()
    #[rustfmt::skip]
    const MAX_VALS: [u128; 126] = [18446744073709551616, 6981463658332, 4294967296, 50859009, 2642246, 319558, 65536, 19113, 7132, 3184, 1626, 921, 566, 371, 256, 185, 139, 107, 85, 69, 57, 48, 41, 35, 31, 27, 24, 22, 20, 18, 16, 15, 14, 13, 12, 12, 11, 10, 10, 9, 9, 8, 8, 8, 7, 7, 7, 7, 6, 6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3];

    if n == 1 {
        return Some(v);
    }

    if n == 0 {
        return Some(1);
    }

    if v == 0 || v == 1 {
        return Some(v);
    }

    if n > (MAX_VALS.len() + 1) as u32 {
        return None;
    }

    // Can't shadow constant `min`
    let mut minimum = 2;
    let mut max = MAX_VALS[n as usize - 2];

    loop {
        let mid = (minimum + max) >> 1;

        let res = mid.pow(n);

        if res > v {
            max = mid;
        } else if res < v {
            minimum = mid + 1;
        } else {
            return Some(mid);
        }

        if minimum == max {
            return None;
        }
    }
}

const fn add(lhs: (i32, u32), rhs: (i32, u32)) -> (i32, u32) {
    simplify((
        lhs.0 * (rhs.1 as i32) + rhs.0 * (lhs.1 as i32),
        lhs.1 * rhs.1,
    ))
}

const fn sub(lhs: (i32, u32), mut rhs: (i32, u32)) -> (i32, u32) {
    rhs.0 = -rhs.0;

    add(lhs, rhs)
}

const fn recip(f: (i128, u128)) -> (i128, u128) {
    simplify_big((f.1 as i128 * f.0.signum(), f.0.unsigned_abs()))
}

const fn mul(lhs: (i128, u128), rhs: (i128, u128)) -> (i128, u128) {
    simplify_big((lhs.0 * rhs.0, lhs.1 * rhs.1))
}

const fn div(lhs: (i128, u128), rhs: (i128, u128)) -> (i128, u128) {
    mul(lhs, recip(rhs))
}

const fn gcd(a: u32, b: u32) -> u32 {
    if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}

const fn gcd_big(a: u128, b: u128) -> u128 {
    if b == 0 {
        a
    } else {
        gcd_big(b, a % b)
    }
}

/// Simplify a fraction that uses 32 bit numbers
pub const fn simplify(v: (i32, u32)) -> (i32, u32) {
    let scale_down = gcd(v.0.unsigned_abs(), v.1);

    (v.0 / (scale_down as i32), v.1 / scale_down)
}

/// Simplify a fraction that uses 128 bit numbers
pub const fn simplify_big(v: (i128, u128)) -> (i128, u128) {
    let scale_down = gcd_big(v.0.unsigned_abs(), v.1);

    (v.0 / (scale_down as i128), v.1 / scale_down)
}

/// Represents SI units
///
/// Each field contains the exponent of the corresponding dimension. Exponents are fractions where the first element of the tuple is the numerator and the second is the denominator. Ensure the fractions are fully simplified if you're creating an instance directly.
#[allow(non_snake_case)]
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct SI {
    /// A scale factor - used for units that are constant multiples of other units; ie. minutes or kilometers
    pub scale: (i128, u128),
    /// Seconds
    pub s: (i32, u32),
    /// Meters
    pub m: (i32, u32),
    /// Kilograms
    pub kg: (i32, u32),
    /// Amperes
    pub A: (i32, u32),
    /// Kelvin
    pub K: (i32, u32),
    /// Moles
    pub mol: (i32, u32),
    /// Candelas
    pub cd: (i32, u32),
}

impl Display for SI {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let mut not_first = false;

        if self.scale.0 != 1
            || self.scale.1 != 1
            || (*self).scale_by(recip(self.scale)) == DIMENSIONLESS
        {
            f.write_str("(")?;
            self.scale.0.fmt(f)?;

            if self.scale.1 != 1 {
                f.write_str("/")?;
                self.scale.1.fmt(f)?;
            }

            f.write_str(")")?;

            not_first = true;
        }

        macro_rules! unit {
            ($name: tt, $signum: expr) => {
                #[allow(unused_assignments)]
                // Implicitly checks for zero too
                if self.$name.0.signum() == $signum {
                    if not_first {
                        f.write_str("⋅")?;
                    }

                    f.write_str(stringify!($name))?;

                    if self.$name.0 != 1 || self.$name.1 != 1 {
                        f.write_str("^")?;
                        self.$name.0.fmt(f)?;

                        if self.$name.1 != 1 {
                            f.write_str("/")?;
                            self.$name.1.fmt(f)?;
                        }
                    }

                    not_first = true;
                }
            };
        }

        // Ensure negative powers come after positive ones

        unit!(s, 1);
        unit!(m, 1);
        unit!(kg, 1);
        unit!(A, 1);
        unit!(K, 1);
        unit!(mol, 1);
        unit!(cd, 1);

        unit!(s, -1);
        unit!(m, -1);
        unit!(kg, -1);
        unit!(A, -1);
        unit!(K, -1);
        unit!(mol, -1);
        unit!(cd, -1);

        Ok(())
    }
}

impl SI {
    /// Multiply the scale factor by a given constant
    ///
    /// ```
    /// # use const_units::units::{second, minute};
    /// assert_eq!(second.scale_by((60, 1)), minute)
    /// ```
    pub const fn scale_by(mut self, scale: (i128, u128)) -> Self {
        self.scale = mul(scale, self.scale);
        self
    }

    /// Multiply SI units
    ///
    /// ```
    /// # use const_units::units::{newton, meter, joule};
    /// assert_eq!(newton.mul(meter), joule)
    /// ```
    pub const fn mul(self, rhs: Self) -> Self {
        SI {
            scale: mul(self.scale, rhs.scale),
            s: add(self.s, rhs.s),
            m: add(self.m, rhs.m),
            kg: add(self.kg, rhs.kg),
            A: add(self.A, rhs.A),
            K: add(self.K, rhs.K),
            mol: add(self.mol, rhs.mol),
            cd: add(self.cd, rhs.cd),
        }
    }

    /// Divide SI units
    ///
    /// ```
    /// # use const_units::units::{joule, second, watt};
    /// assert_eq!(joule.div(second), watt);
    /// ```
    pub const fn div(self, rhs: Self) -> Self {
        SI {
            scale: div(self.scale, rhs.scale),
            s: sub(self.s, rhs.s),
            m: sub(self.m, rhs.m),
            kg: sub(self.kg, rhs.kg),
            A: sub(self.A, rhs.A),
            K: sub(self.K, rhs.K),
            mol: sub(self.mol, rhs.mol),
            cd: sub(self.cd, rhs.cd),
        }
    }

    /// Raise SI units to an integer power
    ///
    /// ```
    /// # use const_units::units::{second, hertz};
    /// assert_eq!(second.powi(-1), hertz);
    /// ```
    pub const fn powi(self, v: i32) -> Self {
        self.powf((v, 1))
    }

    /// Raise SI units to a fractional power
    ///
    /// ```
    /// # use const_units::units::{meter};
    /// assert_eq!(meter.powi(2).powf((1, 2)), meter);
    /// ```
    ///
    /// # Panics
    /// Panics if `scale` can't be raised to the given power without losing precision
    ///
    /// ```should_panic
    /// # use const_units::units::{meter};
    /// meter.scale_by((2, 1)).powf((1, 2)); // `2` isn't a square number
    /// ```
    pub const fn powf(self, v: (i32, u32)) -> Self {
        match self.checked_powf(v) {
            Some(v) => v,
            None => panic!("Scale can't be raised to the given power without losing precision"),
        }
    }

    /// A checked version of `pow`
    ///
    /// ```
    /// # use const_units::units::{meter};
    /// assert_eq!(meter.powi(2).checked_powf((1, 2)), Some(meter));
    /// assert_eq!(meter.scale_by((2, 1)).checked_powf((1, 2)), None);
    /// ```
    pub const fn checked_powf(self, mut v: (i32, u32)) -> Option<Self> {
        v = simplify(v);
        let mut scale = self.scale;

        // `Try` isn't allowed in const
        scale.0 = match try_root(scale.0.unsigned_abs(), v.1) {
            Some(v) => v as i128 * scale.0.signum(),
            None => return None,
        };
        scale.1 = match try_root(scale.1, v.1) {
            Some(v) => v,
            None => return None,
        };

        if v.0.signum() == -1 {
            scale = recip(scale);
        }

        scale.0 = scale.0.pow(v.0.unsigned_abs());
        scale.1 = scale.1.pow(v.0.unsigned_abs());

        Some(SI {
            scale,
            s: simplify((self.s.0 * v.0, self.s.1 * v.1)),
            m: simplify((self.m.0 * v.0, self.m.1 * v.1)),
            kg: simplify((self.kg.0 * v.0, self.kg.1 * v.1)),
            A: simplify((self.A.0 * v.0, self.A.1 * v.1)),
            K: simplify((self.K.0 * v.0, self.K.1 * v.1)),
            mol: simplify((self.mol.0 * v.0, self.mol.1 * v.1)),
            cd: simplify((self.cd.0 * v.0, self.cd.1 * v.1)),
        })
    }

    /// Compare units in constant functions
    pub const fn const_eq(self, other: SI) -> bool {
        self.scale.0 == other.scale.0 && self.scale.1 == other.scale.1 && self.same_dimension(other)
    }

    /// Determine whether two units measure the same dimension
    ///
    /// ```
    /// use const_units::si;
    /// assert!(si("min").same_dimension(si("s")));
    /// assert!(!si("cd").same_dimension(si("s")));
    /// ```
    pub const fn same_dimension(self, other: SI) -> bool {
        self.s.0 == other.s.0
            && self.s.1 == other.s.1
            && self.m.0 == other.m.0
            && self.m.1 == other.m.1
            && self.kg.0 == other.kg.0
            && self.kg.1 == other.kg.1
            && self.A.0 == other.A.0
            && self.A.1 == other.A.1
            && self.K.0 == other.K.0
            && self.K.1 == other.K.1
            && self.mol.0 == other.mol.0
            && self.mol.1 == other.mol.1
            && self.cd.0 == other.cd.0
            && self.cd.1 == other.cd.1
    }
}

#[cfg(test)]
mod tests {
    use crate::{add, gcd, mul, recip, simplify, sub, try_root};

    #[test]
    fn fractional_add() {
        assert_eq!(add((1, 2), (1, 3)), (5, 6));
        assert_eq!(add((1, 2), (-1, 3)), (1, 6));
        assert_eq!(add((-1, 2), (-1, 3)), (-5, 6));
        assert_eq!(add((-2, 2), (-1, 3)), (-4, 3));
        assert_eq!(add((2, 2), (3, 3)), (2, 1));
    }

    #[test]
    fn fractional_sub() {
        assert_eq!(sub((1, 2), (1, 3)), (1, 6));
        assert_eq!(sub((1, 2), (-1, 3)), (5, 6));
        assert_eq!(sub((-1, 2), (-1, 3)), (-1, 6));
        assert_eq!(sub((-2, 2), (-1, 3)), (-2, 3));
        assert_eq!(sub((2, 2), (3, 3)), (0, 1));
    }

    #[test]
    fn fractional_mul() {
        assert_eq!(mul((1, 2), (1, 3)), (1, 6));
        assert_eq!(mul((1, 2), (-1, 3)), (-1, 6));
        assert_eq!(mul((-1, 2), (-1, 3)), (1, 6));
        assert_eq!(mul((-2, 2), (-1, 3)), (1, 3));
        assert_eq!(mul((2, 2), (3, 3)), (1, 1));
        assert_eq!(mul((60, 1), (1, 60)), (1, 1));
    }

    #[test]
    fn test_gcd() {
        assert_eq!(gcd(2, 2), 2);
        assert_eq!(gcd(2, 3), 1);
        assert_eq!(gcd(2, 4), 2);
        assert_eq!(gcd(4, 3), 1);
        assert_eq!(gcd(4, 2), 2);
    }

    #[test]
    fn test_recip() {
        assert_eq!(recip((1, 2)), (2, 1));
        assert_eq!(recip((2, 2)), (1, 1));
        assert_eq!(recip((-1, 2)), (-2, 1));
        assert_eq!(recip((-2, 1)), (-1, 2));
    }

    #[test]
    fn test_simplify() {
        assert_eq!(simplify((5, 6)), (5, 6));
        assert_eq!(simplify((-5, 6)), (-5, 6));
        assert_eq!(simplify((-4, 6)), (-2, 3));
        assert_eq!(simplify((-2, 1)), (-2, 1));
        assert_eq!(simplify((4, 6)), (2, 3));
    }

    #[test]
    fn test_root() {
        assert_eq!(try_root(9, 2), Some(3));
        assert_eq!(try_root(27, 3), Some(3));
        assert_eq!(try_root(28, 3), None);
        assert_eq!(try_root(28, 39), None);
        assert_eq!(try_root(1, 39), Some(1));
        assert_eq!(try_root(28, 0), Some(1));
        assert_eq!(try_root(0, 1), Some(0));
    }
}
